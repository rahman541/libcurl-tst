#include <iostream>
#include <stdio.h>
#include <curl/curl.h>

int main()
{
	CURL *curl = curl_easy_init();
	CURLcode res;
	if (!curl) {
		std::cout << "Cannot init libcurl!\n";
		return 0;
	}

	curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
	res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));

	curl_easy_cleanup(curl);
}