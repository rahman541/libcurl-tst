## Bulding LibCurl
Ref: https://store.chipkin.com/articles/building-libcurl-on-windows-with-mt-and-mtd
1. Dowload [curl](https://curl.haxx.se/download.html)
2. Open `Visual Studio Command Prompt`
3. Run:
```sh
# This will set up the compiler to build for  /MT and /MTd.
Set RTLIBCFG=static
# To build the debug version
nmake /f MakeFile.vc mode=static DEBUG=no MACHINE=x86
nmake /f MakeFile.vc mode=static DEBUG=yes MACHINE=x86
```
